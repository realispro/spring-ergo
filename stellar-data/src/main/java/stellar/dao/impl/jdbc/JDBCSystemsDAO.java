package stellar.dao.impl.jdbc;

import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import stellar.dao.SystemDAO;
import stellar.model.PlanetarySystem;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

//@Repository
public class JDBCSystemsDAO implements SystemDAO {

    public static final Logger logger = Logger.getLogger(JDBCSystemsDAO.class.getName());

    public static final String SELECT_ALL_SYSTEMS = "select ps.id as system_id, " +
            "ps.name as system_name, ps.distance as system_distance, ps.discovery as system_discovery from planetarysystem ps";

    public static final String SELECT_SYSTEMS_BY_NAME = "select ps.id as system_id,  " +
            "ps.name as system_name, ps.discovery as system_discovery, ps.distance as system_distance from planetarysystem ps where name like ?";

    public static final String SELECT_SYSTEM_BY_ID = "select ps.id as system_id,  " +
            "ps.name as system_name, ps.discovery as system_discovery, ps.distance as system_distance from planetarysystem ps where id=?";

    private final JdbcTemplate jdbcTemplate;

    public JDBCSystemsDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<PlanetarySystem> getAllPlanetarySystems() {
        List<PlanetarySystem> systems = jdbcTemplate.query(SELECT_ALL_SYSTEMS, new SystemRowMapper());
        return systems;
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        List<PlanetarySystem> systems = new ArrayList<>();

        return systems;
    }

    @Override
    public PlanetarySystem getPlanetarySystem(int id) {

        try {
            return jdbcTemplate.queryForObject(SELECT_SYSTEM_BY_ID, new SystemRowMapper(), id);
        }catch (EmptyResultDataAccessException e){
            return null;
        }

    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {

        KeyHolder kh = new GeneratedKeyHolder();
        jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                        PreparedStatement stmt = con.prepareStatement(
                                "INSERT INTO PLANETARYSYSTEM (NAME,STAR,DISTANCE,DISCOVERY) VALUES (?,?,?,?)",
                                new String[]{"id"}
                        );
                        stmt.setString(1, system.getName());
                        stmt.setString(2, system.getStar());
                        stmt.setFloat(3, system.getDistance());
                        stmt.setDate(4, new java.sql.Date(system.getDiscovery().getTime()));
                        return stmt;
                    }
                },
                kh
        );

        system.setId(kh.getKey().intValue());

        return system;
    }



}
