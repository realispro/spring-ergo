package stellar.dao.impl.jdbc;

import org.springframework.jdbc.core.RowMapper;
import stellar.model.Planet;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PlanetRowMapper implements RowMapper<Planet> {

    @Override
    public Planet mapRow(ResultSet rs, int rowNum) throws SQLException {
        Planet p = new Planet();
        p.setId(rs.getInt("planet_id"));
        p.setName(rs.getString("planet_name"));
        p.setSize(rs.getInt("planet_size"));
        p.setWeight(rs.getInt("planet_weight"));
        p.setMoons(rs.getInt("planet_moons"));
        return p;
    }
}
