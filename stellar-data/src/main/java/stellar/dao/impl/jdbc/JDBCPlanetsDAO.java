package stellar.dao.impl.jdbc;

import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import stellar.dao.PlanetDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Repository
public class JDBCPlanetsDAO implements PlanetDAO {

    public static final Logger logger = Logger.getLogger(JDBCPlanetsDAO.class.getName());

    public static final String SELECT_ALL_PLANETS = "select p.id as planet_id, p.name as planet_name, " +
            "p.size as planet_size, p.weight as planet_weight, p.moons as planet_moons " +
            "from planet p";

    public static final String SELECT_PLANET_BY_ID = "select p.id as planet_id, p.name as planet_name, " +
            "p.size as planet_size, p.weight as planet_weight, p.moons as planet_moons " +
            "from planet p where id=?";

    public static final String SELECT_PLANETS_BY_SYSTEM = "select p.id as planet_id, p.name as planet_name, " +
            "p.size as planet_size, p.weight as planet_weight, p.moons as planet_moons " +
            "from planet p where system_id=?";

    public static final String SELECT_PLANETS_BY_SYSTEM_AND_NAME = "select p.id as planet_id, p.name as planet_name, " +
            "p.size as planet_size, p.weight as planet_weight, p.moons as planet_moons " +
            "from planet p where system_id=? and name like ?";


    private final DataSource dataSource;
    private final JdbcTemplate jdbcTemplate;

    public JDBCPlanetsDAO(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Planet> getAllPlanets() {
        List<Planet> planets = this.jdbcTemplate.query(SELECT_ALL_PLANETS, new PlanetRowMapper());
        return planets;
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        List<Planet> planets = jdbcTemplate.query(SELECT_PLANETS_BY_SYSTEM, new PlanetRowMapper(), system.getId());

        return planets;
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        return null;
    }

    @Override
    public Planet getPlanetById(int id) {

        return jdbcTemplate.queryForObject(SELECT_PLANET_BY_ID, new PlanetRowMapper(), id);
    }

    @Override
    public Planet addPlanet(Planet p) {
        return null;
    }

}
