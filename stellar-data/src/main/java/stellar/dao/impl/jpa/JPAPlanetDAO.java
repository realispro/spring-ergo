package stellar.dao.impl.jpa;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import stellar.dao.PlanetDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class JPAPlanetDAO implements PlanetDAO {

    @PersistenceContext(unitName = "stellar")
    private EntityManager entityManager;


    @Override
    public List<Planet> getAllPlanets() {
        return entityManager.createQuery("select p from Planet p").getResultList();
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        return entityManager.createQuery("select p from Planet p where p.system = :system")
                .setParameter("system", system)
                .getResultList();
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        return null;
    }

    @Override
    public Planet getPlanetById(int id) {
        return entityManager.find(Planet.class, id);
    }

    @Override
    public Planet addPlanet(Planet p) {
        entityManager.persist(p);
        return p;

    }
}
