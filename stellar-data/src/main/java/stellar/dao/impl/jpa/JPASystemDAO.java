package stellar.dao.impl.jpa;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import stellar.dao.SystemDAO;
import stellar.model.PlanetarySystem;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

//@Repository
public class JPASystemDAO implements SystemDAO {

    @PersistenceContext(unitName = "stellar")
    private EntityManager entityManager;

    @Override
    public List<PlanetarySystem> getAllPlanetarySystems() {
        // JPQL -> HQL -> SQL
        return entityManager
                .createQuery("select s from PlanetarySystem s")
                .getResultList();
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String xyz) {
        return entityManager
                .createQuery("select s from PlanetarySystem s where s.name like :abc")
                .setParameter("abc", xyz)
                .getResultList();
    }

    @Override
    public PlanetarySystem getPlanetarySystem(int id) {
        return entityManager.find(PlanetarySystem.class, id);
    }

    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {
        entityManager.persist(system);
        return system;
    }
}
