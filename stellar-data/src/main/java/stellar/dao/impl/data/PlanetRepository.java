package stellar.dao.impl.data;

import org.springframework.data.jpa.repository.JpaRepository;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.util.List;

public interface PlanetRepository extends JpaRepository<Planet, Integer> {

    List<Planet> findAllBySystem(PlanetarySystem planetarySystem);

    List<Planet> findAllBySystemAndNameContaining(PlanetarySystem planetarySystem, String name);
}