package stellar.dao.impl.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import stellar.dao.SystemDAO;
import stellar.model.PlanetarySystem;

import java.util.List;

public interface SystemRepository extends JpaRepository<PlanetarySystem, Integer>, SystemDAO {

    @Query("select s from PlanetarySystem s where s.name like %:name%")
    List<PlanetarySystem> findAllByNameContaining(@Param("name") String name);

    @Override
    default List<PlanetarySystem> getAllPlanetarySystems(){
        return findAll();
    }

    @Override
    default List<PlanetarySystem> getPlanetarySystemsByName(String like){
        return findAllByNameContaining(like);
    }

    @Override
    default PlanetarySystem getPlanetarySystem(int id){
        return findById(id).orElse(null);
    }

    @Override
    default PlanetarySystem addPlanetarySystem(PlanetarySystem system){
        return save(system);
    }
}
