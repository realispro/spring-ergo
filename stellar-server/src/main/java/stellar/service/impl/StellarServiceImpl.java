package stellar.service.impl;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import stellar.dao.PlanetDAO;
import stellar.dao.SystemDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.List;
import java.util.logging.Logger;

@Service
public class StellarServiceImpl implements StellarService {

    Logger logger = Logger.getLogger(StellarServiceImpl.class.getName());

    private SystemDAO systemDAO;
    private PlanetDAO planetDAO;
    private PlatformTransactionManager transactionManager;


    public StellarServiceImpl(SystemDAO systemDAO, PlanetDAO planetDAO, PlatformTransactionManager transactionManager) {
        this.systemDAO = systemDAO;
        this.planetDAO = planetDAO;
        this.transactionManager = transactionManager;
    }

    @Override
    public List<PlanetarySystem> getSystems() {
        logger.info("fetching all planetary systems");
        List<PlanetarySystem> systems = systemDAO.getAllPlanetarySystems();
        logger.info("found: " + systems.size());
        return systems;
    }

    @Override
    public List<PlanetarySystem> getSystemsByName(String like) {
        logger.info("fetching planetary systems like " + like);
        List<PlanetarySystem> systems =  systemDAO.getPlanetarySystemsByName(like);
        logger.info("found: " + systems.size());
        return systems;
    }

    @Override
    public PlanetarySystem getSystemById(int id) {
        logger.info("fetching planetary system by id " + id);

        PlanetarySystem system = systemDAO.getPlanetarySystem(id);
        //logger.info("found: " + system);
        return  system;
    }

    @Override
    public List<Planet> getPlanets(PlanetarySystem s) {
        logger.info("fetching  planets by system " + s);

        List<Planet> planets = planetDAO.getPlanetsBySystem(s);
        logger.info("found:" + planets.size());
        return planets;
    }

    @Override
    public List<Planet> getPlanets(PlanetarySystem system, String like) {
        return planetDAO.getPlanetsBySystemAndName(system,like);
    }


    @Override
    public Planet getPlanetById(int id) {
        return planetDAO.getPlanetById(id);
    }

    @Override
    public Planet addPlanet(Planet p, PlanetarySystem s) {
        p.setSystem(s);
        return planetDAO.addPlanet(p);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    //@Secured("ROLE_ADMIN") = @RolesAllowed("ROLE_ADMIN")
    @PreAuthorize("hasRole('ADMIN')")
    public PlanetarySystem addPlanetarySystem(PlanetarySystem s) {

       /* TransactionStatus transactionStatus =
                transactionManager.getTransaction(new DefaultTransactionDefinition());
        try {*/
            s = systemDAO.addPlanetarySystem(s);
            if (s.getName().equals("Spring")) {
                throw new IllegalArgumentException("Spring system illegal");
            }
           /* transactionManager.commit(transactionStatus);
        }catch (IllegalArgumentException e){
            transactionManager.rollback(transactionStatus);
            throw e;
        }*/

        return s;
    }

    public void setPlanetDAO(PlanetDAO planetDAO) {
        this.planetDAO = planetDAO;
    }

    public void setSystemDAO(SystemDAO systemDAO) {
        this.systemDAO = systemDAO;
    }
}
