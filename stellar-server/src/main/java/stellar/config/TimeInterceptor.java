package stellar.config;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalTime;

public class TimeInterceptor implements HandlerInterceptor {

    private int opening = 9;
    private int closing = 16;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        LocalTime now = LocalTime.now();
        if(now.getHour()<opening || now.getHour()>=closing){
            response.setStatus(666);
            response.sendRedirect(
                    "https://www.shutterstock.com/image-photo/close-on-red-closed-sign-window-1675369393");
            return false;
        } else {
            return true;
        }

    }

    public void setOpening(int opening) {
        this.opening = opening;
    }

    public void setClosing(int closing) {
        this.closing = closing;
    }
}
