package stellar.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Controller
public class StellarController {

    private static Logger log = Logger.getLogger(StellarController.class.getName());

    private final StellarService service;


    public StellarController(StellarService service) {
        this.service = service;
    }

    @GetMapping("/systems") // ?phrase=abc
    public String getSystems(
            Model model,
            @RequestParam(value = "phrase", required = false) String phrase, //?phrase=abc
            @RequestHeader(value = "User-Agent", required = false) String userAgent,
            @RequestHeader Map<String,String> headers,
            @CookieValue(value = "JSESSIONID", required = false) String sessionId
            ){
        log.info("fetching planetary systems");
        log.info("user-agent: " + userAgent);
        headers.keySet().forEach(
                key->log.info("header: " + key + ":" + headers.get(key)));

        log.info("session id: " + sessionId);

        if("foo".equals(phrase)){
            throw new IllegalArgumentException("Foo not allowed");
        }

        List<PlanetarySystem> systems = phrase==null
                ? service.getSystems()
                : service.getSystemsByName(phrase);

        model.addAttribute("systems", systems);

        return "systems";
    }

    @GetMapping("/planets")
    public String getPlanets(Model model, @RequestParam("systemId") int systemId){
        log.info("fetching planets of the system " + systemId);

        PlanetarySystem system = service.getSystemById(systemId);
        model.addAttribute("system", system);
        model.addAttribute("planets", service.getPlanets(system));

        return "planets";
    }

    @GetMapping("/addSystem")
    public String addSystemPrepare(Model model){
        log.info("preparing system form");
        PlanetarySystem systemForm = new PlanetarySystem();
        systemForm.setName("Unknown");
        model.addAttribute("systemForm", systemForm);
        return "addSystem";

    }

    @PostMapping("/addSystem")
    public String addSystem(
            @ModelAttribute("systemForm") @Validated PlanetarySystem system,
            Errors errors){

        log.info("adding new planetary system " + system);
        // TODO validate first!

        if(errors.hasErrors()){
            return "addSystem"; //jsp
        }

        service.addPlanetarySystem(system);
        return "redirect:/systems"; // url
    }




}
