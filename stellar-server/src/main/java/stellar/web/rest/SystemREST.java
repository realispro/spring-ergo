package stellar.web.rest;

import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

@RestController
@RequestMapping("/webapi")
public class SystemREST {

    private static Logger log = Logger.getLogger(SystemREST.class.getName());

    private final StellarService service;
    private final MessageSource messageSource;

    public SystemREST(StellarService service, MessageSource messageSource) {
        this.service = service;
        this.messageSource = messageSource;
    }


    @GetMapping("/systems")
    public List<PlanetarySystem> getSystems(
            @RequestParam(value = "phrase", required = false) String phrase){
        log.info("fetching systems");

        return phrase==null ? service.getSystems() : service.getSystemsByName(phrase);
    }

    @GetMapping("/systems/{systemId}")
    public ResponseEntity<?> getSystemById(
            @PathVariable("systemId") int systemId){
        log.info("fetching system " + systemId);
        PlanetarySystem system = service.getSystemById(systemId);
        if(system==null){
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("system " + systemId + " not found");
        } else {
            return ResponseEntity.ok(system);
        }
    }

    @PostMapping("/systems")
    public ResponseEntity<?> addSystem(
            @RequestBody @Validated PlanetarySystem system,
            Errors errors,
            Locale locale){
        log.info("adding new system " + system);

        if(errors.hasErrors()){
            StringBuilder sb = new StringBuilder("validation constraint violations");
            errors.getAllErrors().forEach(e->{
                sb.append("\nerror: "
                        + messageSource.getMessage(e.getCode(), e.getArguments(), locale));
            });

            return ResponseEntity
                    .badRequest()
                    .body(sb.toString());
        }

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(service.addPlanetarySystem(system));
    }


    // /systems/:systemId/planets

    @GetMapping("/systems/{systemId}/planets")
    ResponseEntity<List<Planet>> getPlanetsBySystemId(@PathVariable("systemId") int systemId){
        log.info("fetching planets of system " + systemId);

        PlanetarySystem system = service.getSystemById(systemId);

        if(system!=null) {
            List<Planet> planets = service.getPlanets(system);
            return ResponseEntity.status(HttpStatus.OK).body(planets);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
