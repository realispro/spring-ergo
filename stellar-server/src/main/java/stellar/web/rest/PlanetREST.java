package stellar.web.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import stellar.model.Planet;
import stellar.service.StellarService;

import java.util.logging.Logger;

@RestController
@RequestMapping("/webapi")
public class PlanetREST {

    private final Logger log = Logger.getLogger(SystemREST.class.getName());

    private final StellarService service;


    public PlanetREST(StellarService service) {
        this.service = service;
    }


    @GetMapping("/planets/{planetId}")
    public ResponseEntity<?> getPlanetById(@PathVariable("planetId") int planetId) {
        log.info("fetching planet " + planetId);
        Planet planet = service.getPlanetById(planetId);
        if (planet == null) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("planet " + planetId + " not found");
        }
        return ResponseEntity.ok(planet);
    }
}