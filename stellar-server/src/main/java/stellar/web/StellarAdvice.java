package stellar.web;

import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;

import java.util.logging.Level;
import java.util.logging.Logger;

@ControllerAdvice
public class StellarAdvice {

    private static Logger log = Logger.getLogger(StellarAdvice.class.getName());

    private final SystemValidator validator;

    public StellarAdvice(SystemValidator validator) {
        this.validator = validator;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder){
        binder.setValidator(validator);
    }


    @ExceptionHandler(IllegalArgumentException.class)
    public String handleIllegalArgumentException(Model model, IllegalArgumentException e){
        log.log(Level.SEVERE, "handling exception", e);
        model.addAttribute("error_message", e.getMessage());
        return "error";
    }

}
