package stellar.boot.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class HelloREST {

    @GetMapping("/hello")
    String sayHello(@RequestParam(value = "name", required = false) String name){
        name = name!=null ? name : "Unknown";
        return "Hey " + name;
    }

}
