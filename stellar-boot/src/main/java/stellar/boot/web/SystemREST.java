package stellar.boot.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import stellar.boot.data.SystemRepository;
import stellar.boot.model.PlanetarySystem;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/stellar/webapi")
public class SystemREST {

    private final SystemRepository repository;

    @GetMapping("/systems")
    List<PlanetarySystem> getSystems(){
        log.info("retrieving systems");
        return repository.findAll();
    }

    @PostMapping("/systems")
    PlanetarySystem addSystem(@RequestBody PlanetarySystem system){
        log.info("adding system {}", system);
        // TODO validate first!

        return repository.save(system);
    }

}
