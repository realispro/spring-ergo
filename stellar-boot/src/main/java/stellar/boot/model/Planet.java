package stellar.boot.model;

import javax.persistence.*;

@Entity
public class Planet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private int size;
    private int weight;
    private int moons;

    @ManyToOne
    //@JoinColumn(name = "system_id")
    private PlanetarySystem system; // system_id

    public Planet(int id, String name, PlanetarySystem system) {
        this(id, name, 0, 0, 0, system);
    }

    public Planet(int id, String name, int size, int weight, int moons, PlanetarySystem system) {
        this.id = id;
        this.name = name;
        this.size = size;
        this.weight = weight;
        this.moons = moons;
        this.system = system;
    }

    public Planet() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getMoons() {
        return moons;
    }

    public void setMoons(int moons) {
        this.moons = moons;
    }

    public PlanetarySystem getSystem() {
        return system;
    }

    public void setSystem(PlanetarySystem system) {
        this.system = system;
    }

    @Override
    public String toString() {
        return "Planet{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", size=" + size +
                ", weight=" + weight +
                ", moons=" + moons +
                '}';
    }
}
