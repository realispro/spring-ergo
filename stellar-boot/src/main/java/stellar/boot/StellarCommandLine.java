package stellar.boot;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class StellarCommandLine implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        log.info("Hey! from Stellar Boot!");
    }
}
